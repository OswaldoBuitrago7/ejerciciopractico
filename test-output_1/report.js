$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/com/advantageonlineshopping/www/features/registroUsuario.feature");
formatter.feature({
  "name": "Registro de usuario",
  "description": "  Como cliente nuevo, quiero realizar mi registro en la plataforma advantageshopping",
  "keyword": "Característica"
});
formatter.scenario({
  "name": "Registro de usuario exitoso",
  "description": "",
  "keyword": "Escenario",
  "tags": [
    {
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "name": "que se ingresa al portal advantageshopping",
  "keyword": "Dado "
});
formatter.match({
  "location": "RegistroDeUsuarioStepsDefinitions.ingresoAPortalAdvantageShopping()"
});
formatter.result({
  "error_message": "net.serenitybdd.core.exceptions.SerenityManagedException: The following error occurred: unknown error: call function result missing \u0027value\u0027\n  (Session info: chrome\u003d80.0.3987.149)\n  (Driver info: chromedriver\u003d2.33.506120 (e3e53437346286c0bc2d2dc9aa4915ba81d9023f),platform\u003dWindows NT 10.0.18362 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027OSWALDO07\u0027, ip: \u0027192.168.0.20\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_92\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptSslCerts: true, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.33.506120 (e3e53437346286..., userDataDir: C:\\Users\\USER\\AppData\\Local...}, cssSelectorsEnabled: true, databaseEnabled: false, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 80.0.3987.149, webStorageEnabled: true}\nSession ID: 5e0cd151a448df9c71c86c45c27c5100\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.executeScript(RemoteWebDriver.java:489)\r\n\tat net.thucydides.core.webdriver.javascript.JavascriptExecutorFacade.executeScript(JavascriptExecutorFacade.java:61)\r\n\tat net.thucydides.core.pages.jquery.JQueryEnabledPage.executeScriptFrom(JQueryEnabledPage.java:68)\r\n\tat net.thucydides.core.pages.jquery.JQueryEnabledPage.injectJQuery(JQueryEnabledPage.java:60)\r\n\tat net.thucydides.core.pages.jquery.JQueryEnabledPage.activateJQuery(JQueryEnabledPage.java:96)\r\n\tat net.serenitybdd.core.pages.PageObject.addJQuerySupport(PageObject.java:1183)\r\n\tat net.serenitybdd.core.pages.PageObject.openPageAtUrl(PageObject.java:904)\r\n\tat net.serenitybdd.core.pages.PageObject.open(PageObject.java:803)\r\n\tat net.serenitybdd.core.pages.PageObject.open(PageObject.java:791)\r\n\tat com.advantageonlineshopping.www.steps.RegistroDeUsuarioSteps.abrirAplicacion(RegistroDeUsuarioSteps.java:14)\r\n\tat com.advantageonlineshopping.www.steps.RegistroDeUsuarioSteps$$EnhancerByCGLIB$$893e6e44.CGLIB$abrirAplicacion$0(\u003cgenerated\u003e)\r\n\tat com.advantageonlineshopping.www.steps.RegistroDeUsuarioSteps$$EnhancerByCGLIB$$893e6e44$$FastClassByCGLIB$$533f2da8.invoke(\u003cgenerated\u003e)\r\n\tat net.sf.cglib.proxy.MethodProxy.invokeSuper(MethodProxy.java:228)\r\n\tat net.thucydides.core.steps.StepInterceptor.invokeMethod(StepInterceptor.java:460)\r\n\tat net.thucydides.core.steps.StepInterceptor.executeTestStepMethod(StepInterceptor.java:445)\r\n\tat net.thucydides.core.steps.StepInterceptor.runTestStep(StepInterceptor.java:420)\r\n\tat net.thucydides.core.steps.StepInterceptor.runOrSkipMethod(StepInterceptor.java:175)\r\n\tat net.thucydides.core.steps.StepInterceptor.testStepResult(StepInterceptor.java:162)\r\n\tat net.thucydides.core.steps.StepInterceptor.intercept(StepInterceptor.java:68)\r\n\tat com.advantageonlineshopping.www.steps.RegistroDeUsuarioSteps$$EnhancerByCGLIB$$893e6e44.abrirAplicacion(\u003cgenerated\u003e)\r\n\tat com.advantageonlineshopping.www.stepsdefinitions.RegistroDeUsuarioStepsDefinitions.ingresoAPortalAdvantageShopping(RegistroDeUsuarioStepsDefinitions.java:16)\r\n\tat ✽.que se ingresa al portal advantageshopping(src/test/resources/com/advantageonlineshopping/www/features/registroUsuario.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "se procede a ingresar al apartado de usuario",
  "keyword": "Cuando "
});
formatter.match({
  "location": "RegistroDeUsuarioStepsDefinitions.clickInicioUsuario()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "presiona en CREATE NEW ACCOUNT",
  "keyword": "Y "
});
formatter.match({
  "location": "RegistroDeUsuarioStepsDefinitions.precionarRegistroDeUsuario()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "procede a llenar el formulario",
  "keyword": "Y "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});