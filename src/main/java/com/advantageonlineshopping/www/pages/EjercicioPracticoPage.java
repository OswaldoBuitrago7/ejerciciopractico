package com.advantageonlineshopping.www.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import net.serenitybdd.core.pages.PageObject;
//With the next library, I use the annotation @DefaultUrl to go to my website.
import net.thucydides.core.annotations.DefaultUrl;

// url por defecto
@DefaultUrl("https://www.advantageonlineshopping.com/")
public class EjercicioPracticoPage extends PageObject {

	// Ir a formulario registro

	By buttonUsuarioInicio = By.id("menuUser");
	By aCrearNuevaCuenta = By.xpath("/html/body/login-modal/div/div/div[3]/a[2]");

	// Detalle de la cuenta
	By inputUserName = By.name("usernameRegisterPage");
	By inputEmail = By.name("emailRegisterPage");
	By inputPassword = By.name("passwordRegisterPage");
	By inputConfirmPassword = By.name("confirm_passwordRegisterPage");

	// Detalles personales
	By inputFirstName = By.name("first_nameRegisterPage");
	By inputLasName = By.name("last_nameRegisterPage");
	By inputPhoneNumber = By.name("phone_numberRegisterPage");

	// Direccion
	By selectCountry = By.xpath("//*[@id=\"formCover\"]/div[3]/div[1]/sec-view[1]/div/select/option[45]");
	By inputCity = By.name("cityRegisterPage");
	By inputAndres = By.name("addressRegisterPage");
	By inputState = By.name("state_/_province_/_regionRegisterPage");
	By inputPostalCode = By.name("postal_codeRegisterPage");

	// CheckBox Confirmar y Registro
	By checkBoxConditions = By.name("i_agree");
	By buttonRegister = By.id("register_btnundefined");

	// Ingreso al Aplicativo
	By inputLoginUser = By.name("username");
	By inputLoginPassword = By.name("password");
	By buttonLoginIngreso = By.id("sign_in_btnundefined");
	By menuUser = By.id("menuUserLink");

	// Adicionar producto al carrito
	By buttonAddToCart = By.name("save_to_cart");
	By menuCard = By.id("menuCart");

	public void clickInicioUsuario() {
		find(buttonUsuarioInicio).click();
	}

	public void precionarRegistroDeUsuario() {
		find(aCrearNuevaCuenta).click();
	}

	public void detallesDeLaCuenta(String nombreUsuario, String email, String clave, String confirmacionClave) {
		find(inputUserName).sendKeys(nombreUsuario);
		find(inputEmail).sendKeys(email);
		find(inputPassword).sendKeys(clave);
		find(inputConfirmPassword).sendKeys(confirmacionClave);
	}

	public void detallesPersonales(String primerNombre, String apellido, String telefono) {
		find(inputFirstName).sendKeys(primerNombre);
		find(inputLasName).sendKeys(apellido);
		find(inputPhoneNumber).sendKeys(telefono);
		scroll(0, 400);
	}

	public void direccion(String pais, String ciudad, String direccion, String estado, String codigoPostal) {
		find(selectCountry).click();
		find(inputCity).sendKeys("Bogota");
		find(inputAndres).sendKeys("Carrera 110A #152F - 13");
		find(inputState).sendKeys("Colombia");
		find(inputPostalCode).sendKeys("1120281");
	}

	public void registrar() {
		find(checkBoxConditions).click();
		scroll(0, 100);
		find(buttonRegister).click();
		espera(3000);
	}

	public void validarRegistro(String nombreUsuario) {
		// Valida registro
		WebElement lblMensaje = getDriver().findElement(By.xpath("//a[@id='menuUserLink']/span"));
		System.out.println("Nombre Usuario Registrado: " + lblMensaje.getText());
		if (lblMensaje.isDisplayed()) {
			assertThat(lblMensaje.getText(), equalTo(nombreUsuario));
		} else {
			throw new AssertionError("El elemento " + lblMensaje.toString() + " con mensaje '" + nombreUsuario
					+ "' no está presente en la pantalla", null);
		}
	}

	// Inicio de Sesion

	public void ingresaCredencialesDeAcceso(String usuario, String clave) {
		find(inputLoginUser).sendKeys(usuario);
		find(inputLoginPassword).sendKeys(clave);
	}

	public void daClicEnIngresar() {
		find(buttonLoginIngreso).click();
	}

	public void validarLogin(String nombreUsuario) {
		validarRegistro(nombreUsuario);
		find(menuUser).click();
	}

	// Adiciona producto al carrito

	public void seleccionaCategoriaDeProducto(String categoriaProducto) {
		String cProducto = "//span[contains(.,'" + categoriaProducto + "')]";
		find(cProducto).click();
		scroll(0, 300);
		espera(1000);
	}

	public void seleccionaProducto(String nombreProducto) {
		String nProducto = "//a[contains(text(),'" + nombreProducto + "')]";
		find(nProducto).click();
		espera(1000);
	}

	public void adicionaProductoAlCarrito() {
		find(buttonAddToCart).click();
		espera(1000);
	}

	public void validarProductoAdicionadoAlCarrito(String nombreProducto) {
		find(menuCard).click();
		espera(1000);
		
		// Valida producto
		WebElement lblMensaje = getDriver().findElement(By.xpath("//label[contains(.,'" + nombreProducto + "')]"));
		System.out.println("Nombre Producto: " + lblMensaje.getText());
		if (lblMensaje.isDisplayed()) {
			assertThat(lblMensaje.getText(), equalTo(nombreProducto));
		} else {
			throw new AssertionError("El elemento " + lblMensaje.toString() + " con mensaje '" + nombreProducto
					+ "' no está presente en la pantalla", null);
		}
	}

	public void espera(int milisegundos) {
		try {
			Thread.sleep(milisegundos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void scroll(int ParametroInt1, int ParametroInt2) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(" + ParametroInt1 + "," + ParametroInt2 + ")", "");
	}

}
