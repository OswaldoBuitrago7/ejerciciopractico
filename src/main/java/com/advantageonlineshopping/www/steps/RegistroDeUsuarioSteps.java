package com.advantageonlineshopping.www.steps;

import com.advantageonlineshopping.www.pages.EjercicioPracticoPage;

// With the annotation @Step, I will indicate which method will appear in the report
import net.thucydides.core.annotations.Step;

public class RegistroDeUsuarioSteps {
	
	EjercicioPracticoPage registro = new EjercicioPracticoPage();

	@Step
	public void abrirAplicacion() {
		registro.open(); 
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Step
	public void clickInicioUsuario() {
		registro.clickInicioUsuario();
	}
	
	@Step
	public void precionarRegistroDeUsuario() {
		registro.precionarRegistroDeUsuario();
	}
	
	@Step
	public void detallesDeLaCuenta(String nombreUsuario, String email, String clave, String confirmacionClave) {
		registro.detallesDeLaCuenta(nombreUsuario, email, clave, confirmacionClave);
	}
	
	@Step
	public void detallesPersonales(String primerNombre, String apellido, String telefono) {
		registro.detallesPersonales(primerNombre, apellido, telefono);
	}
	
	@Step
	public void direccion(String pais, String ciudad, String direccion, String estado, String codigoPostal) {
		registro.direccion(pais, ciudad, direccion, estado, codigoPostal);
	}
	
	@Step
	public void registrar() {
		registro.registrar();
	}
	
	@Step
	public void validarRegistro(String nombreUsuario) {
		registro.validarRegistro(nombreUsuario);
	}
	
	@Step
	public void ingresaCredencialesDeAcceso(String usuario, String clave) {
		registro.ingresaCredencialesDeAcceso(usuario, clave);
	}
	
	@Step
	public void daClicEnIngresar() {
		registro.daClicEnIngresar();
	}
	
	@Step
	public void validarLogin(String nombreUsuario) {
		registro.validarLogin(nombreUsuario);
	}
	
	@Step
	public void seleccionaCategoriaDeProducto(String categoriaProducto) {
		registro.seleccionaCategoriaDeProducto(categoriaProducto);
	}
	
	@Step
	public void seleccionaProducto(String nombreProducto) {
		registro.seleccionaProducto(nombreProducto);
	}
	
	@Step
	public void adicionaProductoAlCarrito() {
		registro.adicionaProductoAlCarrito();
	}
	
	@Step
	public void validarProductoAdicionadoAlCarrito(String nombreProducto) {
		registro.validarProductoAdicionadoAlCarrito(nombreProducto);
	}
	
	
}
