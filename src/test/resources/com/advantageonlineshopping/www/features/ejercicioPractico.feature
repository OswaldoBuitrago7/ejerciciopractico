#language: es
Característica: Ejercicio Practico
  Como cliente nuevo, quiero realizar mi registro en la plataforma advantageshopping
  Como usuario, quiero iniciar sesion
  Como usuario, quiero adicionar productos al carrito

  @SmokeTest
  Escenario: Dar de alta un usuario.
    Dado que se ingresa al portal advantageshopping
    Cuando se procede a ingresar al apartado de usuario
    Y presiona en CREATE NEW ACCOUNT
    Y procede a llenar el formulario
      | nombreUsuario | email                         | clave        | confirmacionClave | primerNombre | apellido   | telefono   | pais     | ciudad | direccion | estado       | codigoPostal |
      | Oswaldo8      | oswaldobuitrago9911@gmail.com | OswaldoBet99 | OswaldoBet99      | Oswaldo      | Betancourt | 3214737785 | Colombia | Bogota | Carrera 7 | Cundinamarca |      1334535 |
    Entonces valida que se refleje el usuario creado:
      | Oswaldo8 |

  @SmokeTest
  Escenario: Iniciar sesion en el sitio con el usuario creado.
    Dado que se ingresa al portal advantageshopping
    Cuando se procede a ingresar al apartado de usuario
    Y se ingresa usuario y clave
      | Usuario  | Clave        |
      | Oswaldo8 | OswaldoBet99 |
    Y preciona en ingresar
    Entonces valida login exitoso:
      | Oswaldo8 |

  @SmokeTest
  Escenario: Adicionar un elemento al carrito de compras.
    Dado que se ingresa al portal advantageshopping
    Cuando se procede a ingresar al apartado de usuario
    Y se ingresa usuario y clave
      | Usuario  | Clave        |
      | Oswaldo8 | OswaldoBet99 |
    Y preciona en ingresar
    Cuando selecciona una categoria
      | TABLETS |
    Y selecciona un producto
      | HP ElitePad 1000 G2 Tablet |
    Y procede a adicionarlo al carrito
    Entonces valida que le producto se alla adicionado al carrito:
      | HP ELITEPAD 1000 G2 TABLET |