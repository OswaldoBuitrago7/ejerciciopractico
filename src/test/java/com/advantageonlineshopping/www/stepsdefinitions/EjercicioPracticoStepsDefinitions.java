package com.advantageonlineshopping.www.stepsdefinitions;

import com.advantageonlineshopping.www.entities.Registro;
import com.advantageonlineshopping.www.entities.Usuario;
import com.advantageonlineshopping.www.steps.RegistroDeUsuarioSteps;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import java.util.List;

public class EjercicioPracticoStepsDefinitions {
	
	@Steps
	RegistroDeUsuarioSteps registro;
	
	@Dado("^que se ingresa al portal advantageshopping$")
	public void ingresoAPortalAdvantageShopping() {
		registro.abrirAplicacion();
	}

	@Cuando("^se procede a ingresar al apartado de usuario$")
	public void clickInicioUsuario() {
		registro.clickInicioUsuario();
	}

	@Cuando("^presiona en CREATE NEW ACCOUNT$")
	public void precionarRegistroDeUsuario() {
		registro.precionarRegistroDeUsuario();
	}

	@Cuando("^procede a llenar el formulario$")
	public void registroDeUsuarioFormulario(List<Registro> listaUsuario) {
		registro.detallesDeLaCuenta(listaUsuario.get(0).getNombreUsuario(), listaUsuario.get(0).getEmail(), listaUsuario.get(0).getClave(), listaUsuario.get(0).getConfirmacionClave());
		registro.detallesPersonales(listaUsuario.get(0).getPrimerNombre(), listaUsuario.get(0).getApellido(), listaUsuario.get(0).getTelefono());
		registro.direccion(listaUsuario.get(0).getPais(), listaUsuario.get(0).getCiudad(), listaUsuario.get(0).getDireccion(), listaUsuario.get(0).getEstado(), listaUsuario.get(0).getCodigoPostal());
		registro.registrar();
	}
	
	@Entonces("^valida que se refleje el usuario creado:$")
	public void valida(List<String> listNombres) {
		registro.validarRegistro(listNombres.get(0));
	}
	
	@Cuando("^se ingresa usuario y clave$")
	public void ingresaCredencialesDeAcceso(List<Usuario> listaUsuarios) {
		registro.ingresaCredencialesDeAcceso(listaUsuarios.get(0).getUsuario(), listaUsuarios.get(0).getClave());
	}
	
	@Cuando("^preciona en ingresar$")
	public void daClicEnIngresar() {
		registro.daClicEnIngresar();
	}
	
	@Entonces("^valida login exitoso:$")
	public void validaLogin(List<String> listNombres) {
		registro.validarLogin(listNombres.get(0));
	}
	
	// Adicionar Producto al Carrito
	@Cuando("^selecciona una categoria$")
	public void seleccionaCategoriaDeProducto(List<String> listaCategorias) {
		registro.seleccionaCategoriaDeProducto(listaCategorias.get(0));
	}
	
	@Cuando("^selecciona un producto$")
	public void seleccionaProducto(List<String> listaProductos) {
		registro.seleccionaProducto(listaProductos.get(0));
	}
	
	@Cuando("^procede a adicionarlo al carrito$")
	public void adicionaProductoAlCarrito() {
		registro.adicionaProductoAlCarrito();
	}
	
	@Entonces("^valida que le producto se alla adicionado al carrito:$")
	public void validarProductoAdicionadoAlCarrito(List<String> listaMensajes) {
		registro.validarProductoAdicionadoAlCarrito(listaMensajes.get(0));
	}
	
	
	
	
	
	
}
